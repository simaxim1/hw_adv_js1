/* 
1. Поясніть своїми словами, як ви розумієте, як працює прототипне наслідування в Javascript

Кожен новий об'єкт у ієрархії може створюватися, як клон з шаблонного (батьківського) об'єкту та мати всі його властивості та методи,
які, при потребі, можна розширити.

2. Для чого потрібно викликати super() у конструкторі класу-нащадка?

Для виклику/ініціалізації бітьківських властивостей та методів перед тим, як їх перевизначити/розширити у класі-нащадку.

*/

class Employee {
  constructor(name, age, salary) {
    this.name = name;
    this.age = age;
    this.salary = salary;
  }
  
  get name() {
    return this._name;
  }
  
  set name(name) {
    this._name = name;
  }
  
  get age() {
    return this._age;
  }
  
  set age(age) {
    this._age = age;
  }
  
  get salary() {
    return this._salary;
  }
  
  set salary(salary) {
    this._salary = salary;
  }
}
  
class Programmer extends Employee {
  constructor(name, age, salary, lang) {
    super(name, age, salary);
    this.lang = lang;
  }
  
  get lang() {
    return this._lang;
  }
  
  set lang(lang) {
    this._lang = lang;
  }
  
  get salary() {
    return this._salary * 3;
  }

  set salary(salary) {
    this._salary = salary;
  }

  showDetails() {
    console.log(`Programmer: ${this.name};\nAge: ${this.age};\nSalary: ${this.salary}$;\nLanguage: ${this.lang};`);
  }
}
  
const firstProgrammer = new Programmer("Sam", 31, 1000, "C++");
const secondProgrammer = new Programmer("Bob", 25, 2000, "JavaScript");

firstProgrammer.showDetails();
secondProgrammer.showDetails();